# importing textsent tree
# under the alias of ET
import xml.etree.ElementTree as ET
from random import random
from random import seed
import random
import numpy as np
import csv
import matplotlib.pyplot as plt


# PATH: path to the UTxX_updated file
# 
def NoisePopulationInitializer(PATH_1):
                tree = ET.parse(PATH_1)
                root = tree.getroot()
                ##NOISE VALUES UPDATOR:
                #Sector Noise population
                noise_channel_original=list()
                noise_asic_original=list()
                low_thres_original=list()
                zerosupp_thres_org=list()
                cmsnoise_thres_org=list()
                noise_sensore_original=0.0
                for catalogs in tree.findall('catalog'):
                        catalog = catalogs.get('name')
                        if catalog == 'NoiseValues':
                            for conditions in catalogs.findall('condition'):
                                condition = conditions.get('name')
                                if condition == "TestStave":
                                    for texts in conditions:
                                        text = texts.get('name')
                                        if texts.get('name') == "Noise_Channel":                             
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                        noise_channel_original.append(float(i))
                                                
                                        if(texts.attrib['name']=='Low_Threshold'):
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                   low_thres_original.append(float(i))

                                        if(texts.attrib['name']=='High_Threshold'):
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                    zerosupp_thres_org.append(float(i))

                                        if(texts.attrib['name']=='CMS_Noise_Diff'):
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                    cmsnoise_thres_org.append(float(i))
 
                                        if texts.get('name') == "Noise_ASIC":                             
                                                pk=texts.text
                                                l=pk.split()
                                                #print(len(l))
                                                for i in l:
                                                        noise_asic_original.append(float(i))
                        
                                        if texts.get('name') == "Noise_Sensore":                             
                                                pk=texts.text
                                                for i in l:
                                                        noise_sensore_original=float(i)        
                                                 
                ##Pedestal Noise Population                          
                pedestals_org=list()
                pedestals_sigma_org=list()
                for catalogs in tree.findall('catalog'):
                        catalog = catalogs.get('name')
                        if catalog == 'PedestalValues':
                            for conditions in catalogs.findall('condition'):
                                condition = conditions.get('name')
                                if condition == "TestStave":
                                    for texts in conditions:
                                        text = texts.get('name')
                                        if texts.get('name') == "Pedestals_Channel":                             
                                           pk=texts.text
                                           l=pk.split()
                                           for i in l:
                                                pedestals_org.append(int(i))
                                                
                                        if texts.get('name') == "Pedestals_Sigma":                             
                                           pk=texts.text
                                           l=pk.split()
                                          #print(len(l))
                                           for i in l:
                                                pedestals_sigma_org.append(float(i))
                                              
                #CSV File to store initial values of noise and pedestal prior to population
                fields_1 = ['index', 'Noise Values','Low_threshold','High_THreshold','CMS_Noise_Difference','Noise_ASIC','Noise_Sensore','Pedestals','Pedestal_Sigma']   
                # name of csv file 
                filename = "InitialValues.csv"
                # writing to csv file 
                with open(filename, 'w') as csvfile: 
                    #creating a csv writer object 
                    csvwriter = csv.writer(csvfile) 
                    csvwriter.writerow(fields_1)
                    rows=[[]]
                    rows.append([0,noise_channel_original[0],low_thres_original[0], zerosupp_thres_org[0], cmsnoise_thres_org[0] ,noise_asic_original[0],noise_sensore_original,pedestals_org[0],pedestals_sigma_original[0]])
                    for i in range(1,4,1):
                            rows.append([i,noise_channel_original[i],low_thres_original[i], zerosupp_thres_org[i], cmsnoise_thres_org[i], noise_asic_original[i],-0.999,pedestals_org[i],pedestals_sigma_original[i]])
                    for i in range(4,512,1):
                            rows.append([i,noise_channel_original[i],low_thres_original[i], zerosupp_thres_org[i], cmsnoise_thres_org[i],0.999,0.999,pedestals_org[i],-0.999])
                    csvwriter.writerows(rows)
                return pedestals_org, pedestals_sigma_org, noise_channel_original, low_thres_original, zerosupp_thres_org, cmsnoise_thres_org, noise_asic_original, noise_sensore_original  

def NoiseSimulationAccumulator(PATH_2):
                tree = ET.parse(PATH_2)
                root = tree.getroot()
                ##NOISE VALUES UPDATOR:
                #Sector Noise population
                noise_channel_final=list()
                noise_asic_final=list()
                low_thres_final=list()
                zerosupp_thres_final=list()
                cmsnoise_thres_final=list()
                noise_sensore_final=0.0
                for catalogs in tree.findall('catalog'):
                        catalog = catalogs.get('name')
                        if catalog == 'NoiseValues':
                            for conditions in catalogs.findall('condition'):
                                condition = conditions.get('name')
                                if condition == "TestStave":
                                    for texts in conditions:
                                        text = texts.get('name')
                                        if texts.get('name') == "Noise_Channel":                             
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                        noise_channel_final.append(float(i))
                                                
                                        if(texts.attrib['name']=='Low_Threshold'):
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                   low_thres_final.append(float(i))

                                        if(texts.attrib['name']=='High_Threshold'):
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                    zerosupp_thres_final.append(float(i))

                                        if(texts.attrib['name']=='CMS_Noise_Diff'):
                                                pk=texts.text
                                                l=pk.split()
                                                for i in l:
                                                    cmsnoise_thres_final.append(float(i))
 
                                        if texts.get('name') == "Noise_ASIC":                             
                                                pk=texts.text
                                                l=pk.split()
                                                #print(len(l))
                                                for i in l:
                                                        noise_asic_final.append(float(i))
                        
                                        if texts.get('name') == "Noise_Sensore":                             
                                                pk=texts.text
                                                for i in l:
                                                        noise_sensore_final=float(i)        
                                                   


                                           #print(len(noise_sensore_final)) 
                ##Pedestal Noise Population                          
                pedestals_final=list()
                pedestals_sigma_final=list()
                for catalogs in tree.findall('catalog'):
                        catalog = catalogs.get('name')
                        if catalog == 'PedestalValues':
                            for conditions in catalogs.findall('condition'):
                                condition = conditions.get('name')
                                if condition == "TestStave":
                                    for texts in conditions:
                                        text = texts.get('name')
                                        if texts.get('name') == "Pedestals_Channel":                             
                                           pk=texts.text
                                           l=pk.split()
                                           for i in l:
                                                pedestals_final.append(int(i))
                                                
                                        if texts.get('name') == "Pedestals_Sigma":                             
                                           pk=texts.text
                                           l=pk.split()
                                          #print(len(l))
                                           for i in l:
                                                pedestals_sigma_final.append(float(i))
                                              
                #CSV File to store initial values of noise and pedestal prior to population
                fields_1 = ['index', 'Noise Values','Low_threshold', 'High_THreshold','CMS_Noise_Difference','Noise_ASIC','Noise_Sensore','Pedestals','Pedestal_Sigma']   
                # name of csv file 
                filename = "FinalValues.csv"
                # writing to csv file 
                with open(filename, 'w') as csvfile: 
                    #creating a csv writer object 
                    csvwriter = csv.writer(csvfile) 
                    csvwriter.writerow(fields_1)
                    rows=[[]]
                    rows.append([0,noise_channel_final[0],low_thres_final[0], zerosupp_thres_final[0], cmsnoise_thres_final[0] ,noise_asic_final[0],noise_sensore_final,pedestals_final[0],pedestals_sigma_final[0]])
                    for i in range(1,4,1):
                            rows.append([i,noise_channel_final[i],low_thres_final[i], zerosupp_thres_final[i], cmsnoise_thres_final[i], noise_asic_final[i],-0.999,pedestals_final[i],pedestals_sigma_final[i]])
                    for i in range(4,512,1):
                            rows.append([i,noise_channel_final[i],low_thres_final[i], zerosupp_thres_final[i], cmsnoise_thres_final[i],0.999,0.999,pedestals_final[i],-0.999])
                    csvwriter.writerows(rows)
                return pedestals_final, pedestals_sigma_final, noise_channel_final, low_thres_final, zerosupp_thres_final, cmsnoise_thres_final, noise_asic_final, noise_sensore_final 

def NoiseDifferenceTracking(pedestals_final, pedestals_sigma_final, noise_channel_final, low_thres_final, zerosupp_thres_final, cmsnoise_thres_final, noise_asic_final, noise_sensore_final,pedestals_org, pedestals_sigma_org, noise_channel_original, low_thres_original, zerosupp_thres_org, cmsnoise_thres_org, noise_asic_original, noise_sensore_original ):        

                change_nc=[]
                change_lt=[]
                change_zst=[]
                change_cmst=[]                  #sector nooise 
                change_na=[]                #noise asic 
                change_ns=0.0                #sensore 
                list_of_indices_nc = []
                list_of_indices_lt = []
                list_of_indices_zst = []
                list_of_indices_cmst = []
                list_of_indices_na = []
                flag_nc=[]
                flag_lt=[]
                flag_zst=[]
                flag_cmst=[]
                flag_na=[]

               # print("Length of Noise Asic original is  ",len(noise_asic_original))
               # print("Length of Noise Asic final is  ",len(noise_asic_final))
                #print(len(Pedestals_original)==len(Pedestals_final))
              
                for i in range(len(noise_channel_original)): 
                        diff =float(noise_channel_final[i])-float(noise_channel_original[i])
                        if (diff!=0):
                            diff=(diff/(0.5*(float(noise_channel_original[i])+float(noise_channel_final[i]))))*100
                            change_nc.append((diff))
                            list_of_indices_nc.append(i)
                            if(diff>5):
                                flag_nc.append('A')
                            else:
                                flag_nc.append('N')
                               
                for i in range(len(low_thres_original)): 
                        diff =float(low_thres_final[i])-float(low_thres_original[i])
                        if (diff!=0):
                            diff=(diff/(0.5*(float(low_thres_original[i])+float(low_thres_final[i]))))*100
                            change_nc.append((diff))
                            list_of_indices_lt.append(i)
                            if(diff>5):
                                flag_lt.append('A')
                            else:
                                flag_lt.append('N')    

                for i in range(len(zerosupp_thres_original)): 
                        diff =float(zerosupp_thres_final[i])-float(zerosupp_thres_original[i])
                        if (diff!=0):
                            diff=(diff/(0.5*(float(zerosupp_thres_original[i])+float(zerosupp_thres_final[i]))))*100
                            change_zst.append((diff))
                            list_of_indices_zst.append(i)
                            if(diff>5):
                                flag_zst.append('A')
                            else:
                                flag_zst.append('N')
                for i in range(len(cmsnoise_thres_original)): 
                        diff =float(cmsnoise_thres_final[i])-float(cmsnoise_thres_original[i])
                        if (diff!=0):
                            diff=(diff/(0.5*(float(cmsnoise_thres_final[i])+float(cmsnoise_thres_original[i]))))*100
                            change_cmst.append((diff))
                            list_of_indices_cmst.append(i)
                            if(diff>5):
                                flag_cmst.append('A')
                            else:
                                flag_cmst.append('N')                                        
                #print("Sector noise changes length is: ",len(list_of_indices_n))    
                            
                for i in range(len(noise_asic_original)):
                        diff =float(noise_asic_final[i])-float(noise_asic_original[i])
                        if (diff!=0):
                            diff=(diff/(0.5*(noise_asic_original[i]+float(noise_asic_final[i]))))*100
                           # if(abs(diff)>=5):
                            list_of_indices_na.append(i)  
                            change_na.append((diff))
                            if(diff>5):
                                flag_na.append('A')
                            else:
                                flag_na.append('N')      
                print("Changes in Noise ASIC are as follows") 
                print(change_na)
                ##Check for noise sensore change above 5 percent of            
                diff1 =float(noise_sensore_final)-float(noise_sensore_original)
                if (diff1!=0):
                            diff1=(diff1/(0.5*(noise_sensore_original+float(noise_sensore_final))))*100
                            change_s=diff1            

                ##Difference Tracking
                #The difference may be zero for pedestals due to the simulation method and the
                #typecasting to int. As such, there are no checks to ensure the difference is greater than 0             
                #Pedestals
                            
                change_p=[]
                change_ps=[]
                list_of_indices_p = []
                flag_ps=[]
                list_of_indices_ps= [] 
                flag_ps=[]                              
                for i in range(len(Pedestals_original)):
                        diff_float= int(Pedestals_final[i])-Pedestals_original[i]
                        #diff2 = int(Pedestals_final[i])-int(Pedestals_original[i])
                        if(diff_float!=0):
                            diff_float=(diff_float/(0.5*(pedestals_original[i]+int(pedestals_final[i]))))*100 
                            list_of_indices_p.append(i)
                            change_p.append((diff_float))
                            if(diff>5):
                                flag_ps.append('A')
                            else:
                                flag_ps.append('N')

                for i in range(len(cf_original)):
                        diff_float= float(pedestals_sigma_final[i])-float(pedestals_sigma_original[i])
                        if(diff_float!=0):
                            diff_float=(diff_float/(0.5*(float(pedestals_sigma_original[i]+float(pedestals_sigma_final[i])))))*100                  
                            change_ps.append((diff_float))
                            list_of_indices_ps.append(i)                  
                            if(diff>5):
                                flag_ps.append('A')
                            else:
                                flag_ps.append('N')                       

                ##Change Tracker csv
                filename_3 = "Trial_Changes.csv"
                fields_3=['list_of_indices','Noise Changes','flag', 'list_of_indices','LowSuppThres','flag', 'list_of_indices','CMS_Thres','flag', 'list_of_indices','ZeroSuppThres ','flag','list_of_indices','Noise_Asic','flag','Noise_Sensore','list_of_indices','Pedestals','flag', 'list_of_indices','Pedestals_Sigma','flag']
          
                print("Length of Noise Asic changes is  ",len(list_of_indices_na))
                print("Length of Pedestal Sigma changes is  ",len(list_of_indices_ps))
                                         
                #writing to csv file 
                with open(filename_3, 'w') as csvfile: 
                    # creating a csv writer object 
                    csvwriter = csv.writer(csvfile) 
                    csvwriter.writerow(fields_3)
                    rows_2=[[]]
                    print("Length of Sector Noise is: ",len(list_of_indices_n))
                    print("Length of Pedestals is: ",len(list_of_indices_p))
                    print("Length of noise asic is",len(change_na))
                    print("Length of Peestals Sigma is",len(change_ps))

                    rows_2.append([list_of_indices_nc[0], change_nc[0],flag_nc[0], list_of_indices_lt[0],change_lt[0], flag_lt[0], list_of_indices_cmst[0], change_cmst[0], flag_cmst[0], list_of_indices_zst[0], change_zst[0], flag_zst[0], list_of_indices_na[0], change_na[0], flag_na[0],  change_s[0], list_of_indices_p[0], change_p[0], flag_p[0], list_of_indices_ps[0],change_ps[0],flag_ps[0]])
                    for i in range(1,4):
                            rows_2.append([list_of_indices_nc[i], change_nc[i],flag_nc[i], list_of_indices_lt[i],change_lt[i], flag_lt[i], list_of_indices_cmst[i], change_cmst[i], flag_cmst[i], list_of_indices_zst[i], change_zst[i], flag_zst[i], list_of_indices_na[i], change_na[i], flag_na[i],  -0.999, list_of_indices_p[i], change_p[i], flag_p[i], list_of_indices_ps[i],change_ps[i],flag_ps[i]])
                    for i in range(4,10):                        
                            rows_2.append([list_of_indices_nc[i], change_nc[i],flag_nc[i], list_of_indices_lt[i],change_lt[i], flag_lt[i], list_of_indices_cmst[i], change_cmst[i], flag_cmst[i], list_of_indices_zst[i], change_zst[i], flag_zst[i], -0.999, -0.999, '-0.999',  -0.999, list_of_indices_p[i], change_p[i], flag_p[i], -0.999,-0.999,'-0.999'])
                  
                    csvwriter.writerows(rows_2)             
            

PATH_1= "/home/physics/physusers/krupaw/Vetra/TestStave/UTaXHalf0Stave0_updated.xml"
PATH_2= "/home/physics/physusers/krupaw/Vetra/TestStave/UTaXHalf0Stave0_simulated.xml"
[pedestals_org, pedestals_sigma_org, noise_channel_original, low_thres_original, zerosupp_thres_org, cmsnoise_thres_org, noise_asic_original, noise_sensore_original  ]=  NoisePopulationInitializer(PATH_1)
[pedestals_final, pedestals_sigma_final, noise_channel_final, low_thres_final, zerosupp_thres_final, cmsnoise_thres_final, noise_asic_final, noise_sensore_final ]=NoiseSimulationAccumulator(PATH_2)
NoiseDifferenceTracking(pedestals_final, pedestals_sigma_final, noise_channel_final, low_thres_final, zerosupp_thres_final, cmsnoise_thres_final, noise_asic_final, noise_sensore_final,pedestals_org, pedestals_sigma_org, noise_channel_original, low_thres_original, zerosupp_thres_org, cmsnoise_thres_org, noise_asic_original, noise_sensore_original)