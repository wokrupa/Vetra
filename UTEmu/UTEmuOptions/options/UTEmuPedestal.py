__author__ = 'ja'

from Gaudi.Configuration import *
from Configurables import UTEmuOptions


UTEmuOptions().InputFile = '/data2/ut_test-beam/New_Data/data_4x3_200V_0C_correct.raw'
UTEmuOptions().EvtMax=512
UTEmuOptions().Debug = True
UTEmuOptions().isPedestal = True
