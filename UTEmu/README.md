# UTEmu 
The goal of this project is to develop an platform to emulate the performance of UT detector 
The platform is part of VETRA project.


## Getting Started

These instructions will get you a copy of the project up and running on lxplus for development and testing purposes. 


### Installing

This section contains instructions on how to install the software. 
First, you need to clone the repository. 

```
git clone ssh://git@gitlab.cern.ch:7999/lhcb/Vetra.git
```

Then, to build the software, you need to run the following commands: 

```
cd Vetra
lb-project-init
make -j10
```

The dummy data can be generated via frame Generator script:

```
cd UTEmu/UTEmuOptions/scripts
python frameGenerator.py
```

Please use real data sample. Change an input file path in UTEmuPedestal.py  UTEmuRun.py or Configuration.py script.
The data file should contain at least 512 of events to perform proper training of pedestals.
The decoder is adjusted to decode data from 4 ascis (32 bytes frame with last 12 bytes of data)
Output: Root file with decoded adc (4 histograms) + pedestals + cms 

## Running the project

To run the project you can use the following options: 

``` bash
# for pedestal
./build.x86_64-centos7-gcc8-opt/run gaudirun.py UTEmu/UTEmuOptions/options/UTEmuPedestal.py 
# for run
./build.x86_64-centos7-gcc8-opt/run gaudirun.py UT/UTEmuOptions/options/UTEmuRun.py 

```
## Description
Soon

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors

* **Adam Dendek** - *Initial work* 
* **Wojtek Krupa** - *Development* 
See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
