#pragma once


#include "GaudiAlg/GaudiHistoAlg.h"
#include "UTEmu/UTEmuDataType.h"
#include "UTEmu/UTEmuDataLocations.h"
#include <TH1D.h>
#include <TH2D.h>

namespace UTEmu {

class DataMonitorAlgorithm : public GaudiHistoAlg {
 public:
  DataMonitorAlgorithm(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~DataMonitorAlgorithm();

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

 protected:
  enum RunPhase { SKIP, SAVE_SINGLE_EVENTS, REST };

  virtual StatusCode getData(std::string);
  virtual StatusCode initializeBase();

  virtual StatusCode skippEvent();
  virtual StatusCode saveSimpleEvents();
  virtual StatusCode fillOnly2DHistogram();

  virtual RunPhase getRunPhase();
  virtual void createHistogram2D();

  virtual void storeEventIntoHistogram(unsigned int);
  virtual TH1D* bookHistogram(const std::string& p_histogramName,
                              const std::string& p_histogramTitle,
                              int p_sensorNumber);
  virtual TH2D* bookHistogram2D(const std::string& p_histogramName,
                                const std::string& p_histogramTitle,
                                int p_sensorNumber);

  virtual std::string createHistogramTitle(unsigned int);
  virtual std::string createHistogramName(unsigned int);
  virtual void fillHistogram(TH1D* p_histogram);
  virtual void fillHistogram2D(unsigned int);
  virtual void buildProjectionHistogram();

  std::string m_outpuProjectionHistogramName;
  int m_displayEventNumber;
  int m_evtNumber;
  int m_skipEvent;
  TH2D* m_histogram2D[UTEmu::DataLocations::number_of_asics];
  std::string m_input;
  bool m_debug;
  unsigned int m_numOfASICs;
  UTEmu::RawDataMap<>* m_dataMap;
  UTEmu::RawData<> m_data;
};
}
