#include "UTEmuDataMonitorAlgorithm.h"
#include "GaudiUtils/Aida2ROOT.h"

using namespace UTEmu;
DECLARE_COMPONENT(UTEmu::DataMonitorAlgorithm)

DataMonitorAlgorithm::DataMonitorAlgorithm(const std::string &name,
                                           ISvcLocator *pSvcLocator)
    : GaudiHistoAlg(name, pSvcLocator),

      m_outpuProjectionHistogramName(),
      m_displayEventNumber(0),
      m_evtNumber(0),
      m_skipEvent(0),
      m_histogram2D(),
      m_input(),
      m_debug(),
      m_numOfASICs()
{
  declareProperty("displayEventNumber", m_displayEventNumber = 20);
  declareProperty("skippEventNumber", m_skipEvent = 0);
  declareProperty("inputData", m_input = UTEmu::DataLocations::RawTES);
  declareProperty("Debug", m_debug = false);
  declareProperty("numOfASICs", m_numOfASICs = UTEmu::DataLocations::number_of_asics);

}

DataMonitorAlgorithm::~DataMonitorAlgorithm() {}

StatusCode DataMonitorAlgorithm::initialize()
{
  if (StatusCode::SUCCESS != initializeBase())
    return StatusCode::FAILURE;
  createHistogram2D();
  return StatusCode::SUCCESS;
}

StatusCode DataMonitorAlgorithm::execute()
{

  RunPhase l_runPhase = getRunPhase();
  switch (l_runPhase)
  {
  case SKIP:
    return skippEvent();
  case SAVE_SINGLE_EVENTS:
    return saveSimpleEvents();
  default:
    return fillOnly2DHistogram();
  }
}
StatusCode DataMonitorAlgorithm::finalize()
{
  buildProjectionHistogram();
  return GaudiHistoAlg::finalize();
}

StatusCode DataMonitorAlgorithm::initializeBase()
{
  return GaudiHistoAlg::initialize();
}

void DataMonitorAlgorithm::createHistogram2D()
{
  for (unsigned int asic_num = 0; asic_num < m_numOfASICs; asic_num++)
  {
    std::string l_histogramName = "RawData_vs_channel_" + std::to_string(asic_num );
    std::string l_histogramTtttle = "RawData_vs_channel_" + std::to_string(asic_num );
    int l_sensorNum = RawData<>::getnChannelNumber();
    m_histogram2D[asic_num] =
        bookHistogram2D(l_histogramName, l_histogramTtttle, l_sensorNum);
  }
}

StatusCode DataMonitorAlgorithm::getData(std::string input_loc)
{
  m_dataMap = getIfExists<RawDataMap<>>(input_loc);
  if (!m_dataMap)
  {
    error() << " ==> There is no input data: " << input_loc << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

StatusCode DataMonitorAlgorithm::skippEvent()
{
  m_evtNumber++;
  return StatusCode::SUCCESS;
}

StatusCode DataMonitorAlgorithm::saveSimpleEvents()
{
  setHistoDir(const_cast<char *>("UTEmu__RawDataMonitorAlgorithm_Events/"));

  if (StatusCode::SUCCESS != getData(m_input))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
    return StatusCode::SUCCESS;
  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = RawData<>(Data);
    storeEventIntoHistogram(asic_num);
    fillHistogram2D(asic_num);
  }
  m_evtNumber++;
  return StatusCode::SUCCESS;
}

StatusCode DataMonitorAlgorithm::fillOnly2DHistogram()
{
  setHistoDir(const_cast<char *>("UTEmu/"));
  if (StatusCode::SUCCESS != getData(m_input))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
    return StatusCode::SUCCESS;
  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = RawData<>(Data);
    fillHistogram2D(asic_num);
  }
  m_evtNumber++;
  return StatusCode::SUCCESS;
}

void DataMonitorAlgorithm::storeEventIntoHistogram(unsigned int asic_num)
{
  std::string l_histName = createHistogramName(asic_num);
  std::string l_histTitle = createHistogramTitle(asic_num);
  int l_sensorNum = RawData<>::getnChannelNumber();
  TH1D *l_eventHist = bookHistogram(l_histName, l_histTitle, l_sensorNum);
  fillHistogram(l_eventHist);
}

std::string DataMonitorAlgorithm::createHistogramName(unsigned int asic_num)
{
  return "event_" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num );
}

std::string DataMonitorAlgorithm::createHistogramTitle(unsigned int asic_num)
{
  return "Raw Data_" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num );
}

TH1D *DataMonitorAlgorithm::bookHistogram(const std::string &p_histogramName,
                                          const std::string &p_histogramTitle,
                                          int p_sensorNumber)
{
  return Gaudi::Utils::Aida2ROOT::aida2root(book1D(
      p_histogramName, p_histogramTitle, -0.5 + RawData<>::getMinChannel(),
      RawData<>::getMaxChannel() - 0.5, p_sensorNumber));
}

void DataMonitorAlgorithm::fillHistogram(TH1D *p_histogram)
{
  int channelNumber = RawData<>::getnChannelNumber();
  for (int chan = 0; chan < channelNumber; chan++)
  {
    p_histogram->SetBinContent(chan, m_data.getSignal(chan));
  }
}

TH2D *DataMonitorAlgorithm::bookHistogram2D(const std::string &p_histogramName,
                                            const std::string &p_histogramTitle,
                                            int p_sensorNumber)
{
  int l_ylow = -35;
  int l_yhigh = 35;
  int l_ybin = 70;
  return Gaudi::Utils::Aida2ROOT::aida2root(book2D(
      p_histogramName, p_histogramTitle, -0.5 + RawData<>::getMinChannel(),
      RawData<>::getMaxChannel() - 0.5, p_sensorNumber, l_ylow, l_yhigh,
      l_ybin));
}

void DataMonitorAlgorithm::fillHistogram2D(unsigned int asic_num)
{
  int channelNumber = RawData<>::getnChannelNumber();
  for (int chan = 0; chan < channelNumber; chan++)
  {
    auto channelSignal = m_data.getSignal(chan);
    m_histogram2D[asic_num]->Fill(chan + RawData<>::getMinChannel(), channelSignal);
  }
}

DataMonitorAlgorithm::RunPhase DataMonitorAlgorithm::getRunPhase()
{
  if (m_evtNumber < m_skipEvent)
    return SKIP;
  if (m_evtNumber < m_displayEventNumber + m_skipEvent)
    return SAVE_SINGLE_EVENTS;
  else
    return REST;
}

void DataMonitorAlgorithm::buildProjectionHistogram()
{
  for (unsigned int asic_num = 0; asic_num < m_numOfASICs; asic_num++)
  {
    m_histogram2D[asic_num]->ProjectionY((m_outpuProjectionHistogramName + "_" + std::to_string(asic_num )).c_str());
  }
}
