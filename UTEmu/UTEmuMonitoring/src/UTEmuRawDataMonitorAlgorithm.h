/*
 * UTEmuRawDataMonitorAlgorithm.h
 *
 *  Created on: Oct 7, 2014
 *      Author: ADendek
 */

#pragma once

#include "UTEmuDataMonitorAlgorithm.h"

namespace UTEmu {

class RawDataMonitorAlgorithm : public DataMonitorAlgorithm {
 public:
  RawDataMonitorAlgorithm(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode finalize() override;
};
}
