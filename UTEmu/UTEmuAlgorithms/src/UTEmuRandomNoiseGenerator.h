/*
 * UTEmuRandomNoiseGenerator.h
 *
 *  Created on: Mar 16, 2015
 *      Author: ADendek
 */

#pragma once

#include "UTEmuIDataReader.h"
#include <TRandom.h>

namespace UTEmu {

class RandomNoiseGenerator : public IDataReader {
 public:
  RandomNoiseGenerator(double& p_sigma, double& p_mean);
  void checkInput() override;
  std::array<RawData<>, 4>* getEventData() override;

 private:
  double& m_sigma;
  double& m_mean;
  TRandom m_randomGenerator;
};

} /* namespace UTEmu */
