/*
 * UTEmuICommonModeSubtractor.h
 *
 *  Created on: Nov 23, 2014
 *      Author: ADendek
 */
#pragma once

#include "UTEmuIProcessingEngine.h"

namespace UTEmu {

class ICommonModeSubtractor : public IProcessingEngine<int, double> {
 public:
  virtual ~ICommonModeSubtractor(){};
};
}
