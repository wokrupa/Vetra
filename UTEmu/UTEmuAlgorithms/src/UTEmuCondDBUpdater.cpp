
#include "UTEmuCondDBUpdater.h"
#include "UTEmu/UTEmuDataLocations.h"

using namespace UTEmu;

DECLARE_COMPONENT(UTEmu::CondDBUpdater)

CondDBUpdater::CondDBUpdater(
    const std::string &name, ISvcLocator *pSvcLocator)
    : GaudiAlgorithm(name, pSvcLocator),
      m_file(),
      m_histo(),
      m_xmlFileName(),
      m_inputFileName(),
      m_HistoFileValidator(m_inputFileName),
      m_numofAsics()
{
    declareProperty("CondDBFileName", m_xmlFileName = "");
    declareProperty("HistoInputFileName", m_inputFileName = "");
    declareProperty("NumberofAsics", m_numofAsics = UTEmu::DataLocations::number_of_asics);
}

StatusCode CondDBUpdater::initialize()
{

    if (m_HistoFileValidator.validateFile())
    {
        m_file = new TFile(m_inputFileName.c_str());
        return StatusCode::SUCCESS;
    }
    else
    {
        error() << " ==> There is no input noise dictionary. Check input! " << m_inputFileName << endmsg;
        return StatusCode::FAILURE;
    }
}

StatusCode CondDBUpdater::execute()
{
    for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
    {
        for (unsigned int channel = 0; channel < 128; channel++)
        {
            std::string histo_name = "UTEmu__CommonModeSubtractorDataMonitorAlgorithm/Noise_channel_"+std::to_string(asic_num)+"_"+std::to_string(channel)+";1";
            m_histo = (TH1F *)m_file->Get(histo_name.c_str());
            std::cout << m_histo->GetMean() << std::endl;
        }
    }
    return StatusCode::SUCCESS;
}

StatusCode CondDBUpdater::finalize()
{

    return StatusCode::SUCCESS;
}
