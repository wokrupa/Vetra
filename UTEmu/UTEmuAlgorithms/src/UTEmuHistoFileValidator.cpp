/*
 * UTEmuHistoFileValidator.cpp
 *
 *  Created on: Jan 2, 2015
 *      Author: ADendek
 */

#include "UTEmuHistoFileValidator.h"

using namespace UTEmu;
using namespace std::filesystem;

HistoFileValidator::HistoFileValidator(const std::string& p_filename)
    : m_filename(p_filename) {}

bool HistoFileValidator::validateFile() {
  m_path = path(m_filename);
  bool l_result = isfileExist() && isRegularFile() && hasNonZeroSize();
  return l_result;
}

bool HistoFileValidator::isfileExist() { return exists(m_path); }

bool HistoFileValidator::isRegularFile() { return is_regular_file(m_path); }

bool HistoFileValidator::hasNonZeroSize() {
  return !(0 == file_size(m_path));
}
