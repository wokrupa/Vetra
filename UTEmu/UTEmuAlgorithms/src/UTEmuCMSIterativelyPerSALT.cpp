#include "UTEmuCMSIterativelyPerSALT.h"
#include <iostream>
#include <cmath>

using namespace UTEmu;
using namespace std;

CMSIterativelyPerSALT::CMSIterativelyPerSALT(
    IChannelMaskProvider& p_masksProvider)
    : m_masksProvider(p_masksProvider),
      m_channelNumber(RawData<>::getnChannelNumber()),
      m_channelPerSALT(128) {
  cout << "create iteratively correlation per SALT" << endl;
  initializeCorrectionAndHitMaps();
}

void CMSIterativelyPerSALT::initializeCorrectionAndHitMaps() {
  for (int channel = 0; channel < m_channelNumber;
       channel += m_channelPerSALT) {
    m_correctionPerSALT.insert(std::make_pair(channel, 0));
    m_hitThresholdPerSALT.insert(std::make_pair(channel, 200));
  }
}

void CMSIterativelyPerSALT::processEvent(RawData<>* p_data,
                                           RawData<double>** p_output) {
  int numberOfIteration = 2;
  RawData<double>* tmpData;
  // first iteration
  calculateCorrection<int>(p_data);
  removeCM<int, double>(p_data, p_output);
  // rest of the iterations
  for (int iteration = 1; iteration < numberOfIteration; iteration++) {
    tmpData = new RawData<double>(**p_output);
    calculateCorrection<double>(tmpData);
    removeCM<double, double>(tmpData, p_output);
  }
  resetHitThresholds();
}

template <typename DATA_TYPE>
void CMSIterativelyPerSALT::calculateCorrection(
    RawData<DATA_TYPE>* p_inputData) {
  for (auto& mapIt : m_correctionPerSALT) {
    int usedChannels = 0;
    double rmsPerSALT = 0;
    for (int channel = mapIt.first; channel < mapIt.first + m_channelPerSALT;
         channel++) {
      double signal = static_cast<double>(p_inputData->getSignal(channel));
      if (!m_masksProvider.isMasked(channel) &&
          abs(signal) < m_hitThresholdPerSALT[mapIt.first]) {
        mapIt.second += signal;
        rmsPerSALT += signal * signal;
        usedChannels++;
      }
    }
    if (usedChannels) mapIt.second /= static_cast<double>(usedChannels);
    if (usedChannels) rmsPerSALT /= static_cast<double>(usedChannels);
    rmsPerSALT -= mapIt.second * mapIt.second;
    double rmsMultiplicity = 4;
    m_hitThresholdPerSALT[mapIt.first] = rmsMultiplicity * sqrt(rmsPerSALT);
  }
}

template <typename INPUT_DATA_TYPE, typename OUTPUT_TYPE_NAME>
void CMSIterativelyPerSALT::removeCM(RawData<INPUT_DATA_TYPE>* p_data,
                                       RawData<OUTPUT_TYPE_NAME>** p_output) {
  for (auto& mapIt : m_correctionPerSALT) {
    for (int channel = mapIt.first; channel < mapIt.first + m_channelPerSALT;
         channel++) {
      if (m_masksProvider.isMasked(channel)) {
        OUTPUT_TYPE_NAME signalMaskedChannel = 0;
        (*p_output)->setSignal(signalMaskedChannel);
      } else {
        OUTPUT_TYPE_NAME l_channelSignal =
            p_data->getSignal(channel) - mapIt.second;
        (*p_output)->setSignal(l_channelSignal);
      }
    }
  }
  (*p_output)->setTDC(p_data->getTDC());
  (*p_output)->setTime(p_data->getTime());
}

void CMSIterativelyPerSALT::resetHitThresholds() {
  for (auto& mapIt : m_hitThresholdPerSALT) mapIt.second = 200;
}
