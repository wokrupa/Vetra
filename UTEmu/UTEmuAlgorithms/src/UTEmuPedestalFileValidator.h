/*
 * UTEmuPedestalFileValidator.h
 *
 *  Created on: Jan 2, 2015
 *      Author: ADendek
 */

#include "UTEmuIFileValidator.h"
#include <filesystem>
#include <string>

namespace UTEmu {

class PedestalFileValidator : public IFileValidator {
 public:
  PedestalFileValidator(const std::string& p_filename);
  bool validateFile() override;

 private:
  bool isfileExist();
  bool isRegularFile();
  bool hasNonZeroSize();

  const std::string& m_filename;
  std::filesystem::path m_path;
};

} /* namespace UTEmu */
