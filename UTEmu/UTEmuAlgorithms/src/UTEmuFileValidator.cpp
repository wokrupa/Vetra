/*
 * UTEmuFileValidator.cpp
 *
 *  Created on: Oct 5, 2014
 *      Author: ADendek
 */
#include "UTEmuFileValidator.h"
#include <string>
#include <iostream>
using namespace UTEmu;
using namespace std::filesystem;
using namespace std;

FileValidator::FileValidator(std::string& p_filename)
    : m_filename(p_filename) {}

bool FileValidator::validateFile() {
  m_path = path(m_filename);
  bool l_result = isfileExist() && isRegularFile() && hasNonZeroSize();
  return l_result;
}

bool FileValidator::isfileExist() { return exists(m_path); }

bool FileValidator::isRegularFile() { return is_regular_file(m_path); }

bool FileValidator::hasNonZeroSize() { return !(0 == file_size(m_path)); }
