/*
 *  UTEmuRawData<>Factory.h
 * 
 *      Created   : March, 2020
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#pragma once

#include "UTEmuIDataReader.h"
#include "UTEmu/UTEmuDataType.h"
#include "UTEmuFileValidator.h"
#include <memory>

namespace UTEmu {

namespace InputDataOption {
static const std::string& NoiseGenerator = "Generator";
static const std::string& Binary = "Binary";
static const std::string& TxT = "TxT";

}

class RawDataFactory {
 public:
  typedef std::shared_ptr<IDataReader> DataReaderPtr;

  class NoSuchState : public std::runtime_error {
   public:
    NoSuchState(const std::string& p_errorMsg)
        : std::runtime_error(p_errorMsg) {}
  };
  
  RawDataFactory(std::string& p_filename,
                 IFileValidator& p_fileValidator, unsigned int& p_numOfASICs,
                 double& p_mean, double& p_sigma);
  DataReaderPtr createDataEngine(const std::string& p_inputDataOption);

 private:
  std::string& m_filename;
  IFileValidator& m_fileValidator;
  unsigned int& m_numOfASICs;
  double& m_mean;
  double& m_sigma;

};

} /* namespace UTEmu */
