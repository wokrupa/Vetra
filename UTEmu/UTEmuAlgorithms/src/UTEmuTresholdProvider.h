/*
 * UTEmuTresholdProvider.h
 *
 *  Created on: Jan 4, 2015
 *      Author: ADendek
 */

#pragma once

#include "UTEmuITresholdProvider.h"
#include "UTEmu/UTEmuNoise.h"
#include <string>

namespace UTEmu {

class TresholdProvider : public ITresholdProvider {
 public:
  TresholdProvider(const std::string& p_noiseFile,
                   const double& p_lowThresholdMultiplicity,
                   const double& p_highThresholdMultiplicity);

  void retreiveTresholds() override;
  double getLowClusterThreshold(int p_channel) override;
  double getHighClusterThreshold(int p_channel) override;

 private:
  const std::string& m_noiseFile;
  const double& m_lowThresholdMultiplicity;
  const double& m_highThresholdMultiplicity;
  Noise m_noise;
};

} /* namespace UTEmu */
