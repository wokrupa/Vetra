
from Gaudi.Configuration import *
from Configurables import Brunel, GaudiSequencer
from Configurables import  PrTrackChecker, PrForwardTracking
from GaudiConf import IOHelper


FilePrefOut="test_ntuple"
myOutputFilePref="Brunel"+FilePrefOut
HistogramPersistencySvc().OutputFile= myOutputFilePref+"-histo_test12.root"
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='VeloOccupancyTuple.root' TYP='ROOT' OPT='NEW'"]

from Configurables import VPClus
#VPClus().OutputLevel=DEBUG

brunel = Brunel()
brunel.DataType = "Upgrade"
brunel.EvtMax = -1
brunel.PrintFreq = 1000
brunel.WithMC = True
brunel.OutputType = "NONE"
brunel.Simulation = True
brunel.InputType = 'DIGI'
brunel.DDDBtag    = "dddb-20190223"
brunel.CondDBtag  = "sim-20180530-vc-mu100"
#brunel.OutputLevel=DEBUG
brunel.MainSequence = ['ProcessPhase/Reco', 'ProcessPhase/MCLinks', 'ProcessPhase/Check']

from Configurables import RecombineRawEvent
RecombineRawEvent().Version = 4.2

#from Configurables import FTRawBankDecoder
#FTRawBankDecoder("createFTClusters").DecodingVersion = 6

def ConfGaudiSeq():
    GaudiSequencer("CaloFutureBanksHandler").Members = []
    GaudiSequencer("CaloBanksHandler").Members = []
    GaudiSequencer("MCLinksCaloSeq").Members = []
    GaudiSequencer("RecoRICHSeq").Members = []
    GaudiSequencer("RecoCALOFUTURESeq").Members = []
    GaudiSequencer("RecoMUONSeq").Members = []
    GaudiSequencer("RecoPROTOSeq").Members = []
    GaudiSequencer("RecoSUMMARYSeq").Members = []
    GaudiSequencer("LumiSeq").Members = []
    GaudiSequencer("RecoTrBestSeq").Members = []
    GaudiSequencer("CheckRICHSeq").Members = [ ]
    GaudiSequencer("CheckMUONSeq").Members = [ ]
    #GaudiSequencer("VPMonitor").Members = []
    
    
    GaudiSequencer("RecoTrFastSeq").Members = GaudiSequencer("RecoTrFastSeq").Members[:-1]
    from Configurables import VPClusFull
    GaudiSequencer("RecoTrFastSeq").Members +=[VPClusFull()]
    
    GaudiSequencer("MCLinksTrSeq").Members.pop(4)
    GaudiSequencer("MCLinksTrSeq").Members.pop(5)
    GaudiSequencer("MCLinksTrSeq").Members.pop(5)
    GaudiSequencer("MCLinksTrSeq").Members.pop(6)
    GaudiSequencer("MCLinksTrSeq").Members.pop(6)
    GaudiSequencer("MCLinksTrSeq").Members.pop(6)
    GaudiSequencer("MCLinksTrSeq").Members.pop(6)
    
    GaudiSequencer("CheckPatSeq").Members  = GaudiSequencer("CheckPatSeq").Members[:3]

    from Configurables import TestVPMonitor
    moni = TestVPMonitor("VPMonitor")
    GaudiSequencer("CheckPatSeq").Members.append(moni) 
            
     
appendPostConfigAction(ConfGaudiSeq)

files = [

    #"root://lhcb-sdpd9.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/DIGI/00091129/0000/00091129_00000001_1.digi"
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091069_00000033_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091129_00000134_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091069_00000023_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091129_00000011_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091069_00000041_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091069_00000018_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091129_00000193_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091129_00000147_1.digi",
    #"root://eoslhcb.cern.ch//eos/lhcb/user/a/adavis/00091069_00000016_1.digi",
    #"root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/DIGI/00091129/0000/00091129_00000001_1.digi",
    "root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/DIGI/00091129/0000/00091129_00000001_1.digi", 
]


IOHelper("ROOT").inputFiles(files)
